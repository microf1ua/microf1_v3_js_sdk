import axios from 'axios';

const DepositApplicationStatus = {
    STATUS_NEW: 1,
    STATUS_WAITING_BORROWER_ASSIGN: 2,
    STATUS_WAITING_DECISION: 3,
    STATUS_APPROVED: 4,
    STATUS_CANCELED: 5,
    STATUS_SENT_FOR_REVISION: 6,
    STATUS_WAITING_MONEY_TRANSFER: 7,
    STATUS_DONE: 8,
};

const LoanStatus = {
    STATUS_NEW: 1,
    STATUS_WAITING_BORROWER_ASSIGN: 2,
    STATUS_WAITING_DECISION: 3,
    STATUS_APPROVED: 4,
    STATUS_CANCELED: 5,
    STATUS_SENT_FOR_REVISION: 6,
    STATUS_WAITING_MONEY_TRANSFER: 7,
    STATUS_DONE: 8,
};

const CreditStatus = {
    STATUS_OPEN: 1,
    STATUS_PROLONGED: 2,
    STATUS_RESTRUCTURED: 3,
    STATUS_OVERDUE: 4,
    STATUS_OVERDUE_RESTRUCTURED: 5,
    STATUS_SOLD: 6,
    STATUS_CLOSED: 7,
};

const DocumentTemplateTypes = {
    TYPE_CONTRACT: 1,
    TYPE_INCOMING_CASH_ORDER: 2,
    TYPE_OUTGOING_CASH_ORDER: 3,
    TYPE_CASH_TRANSFER_ACT: 4,
    TYPE_CREDIT_DETAILS: 5,
    TYPE_CONTRACT_PROLONGATION: 6,
    TYPE_CREDIT_UNION_APPLICATION: 7,
    TYPE_APPLICATION_PUBLIC_ORGANIZATION: 8,
    TYPE_PA_CREDIT_UNION: 9,
    TYPE_DEPOSIT_CONTRACT: 10,
    TYPE_CHANGE_POSTAL_ADDRESS_APPLICATION: 11,
    TYPE_CHANGE_CREDIT_CARD_APPLICATION: 12,
    TYPE_APPLICATION_EXIT_PUBLIC_ORGANIZATION: 13,
    TYPE_EXIT_CREDIT_UNION_APPLICATION: 14,
    TYPE_RETURN_DEPOSIT_APPLICATION: 15,
    TYPE_CHANGE_PLACE_REGISTRATION_APPLICATION: 16,
    TYPE_CHANGE_PHONE_NUMBER_APPLICATION: 17,
    TYPE_CHANGE_PASSPORT_APPLICATION: 18,
    TYPE_COMPRONISE_CEP_APPLICATION: 19,
};

const DepositPayoutScheduleStatus = {
    STATUS_NOT_PAID: 0,
    STATUS_PAID_OUT: 1,
    STATUS_PAYMENT_ERROR: 2,
};

const ResponseHandler = {
    dispatch(response) {
        if (response[0].code === 0) {
            return {
                result: response[0],
                data: response[0].data,
                status: true,
            };
        } else {
            return {
                data: response,
                status: false,
            };
        }
    },
};

class ManagerSDK {
    constructor(token, baseUrl, unitId, language) {
        axios.defaults.headers.common.Accept = 'application/json';
        axios.defaults.headers.common['Content-Type'] = 'application/json';

        if (token) {
            axios.defaults.headers.common.Authorization = `Bearer ${token}`;
            axios.defaults.headers.common['MF-UNIT-ID'] = unitId;
        }

        if (language) axios.defaults.headers.common['Language'] = language;

        this.axios = axios.create({
            baseURL: baseUrl,
            responseType: 'json',
        });
    }

    getAxios() {
        return this.axios;
    }

    getDepositProducts() {
        return this.getAxios().post('/deposit/deposit-product/list');
    }

    approveDepositPayoutSchedule(params) {
        return this.getAxios().post('/deposit/deposit-payout-schedule/approve', params);
    }
}

class BorrowerSDK {
    constructor(token, baseUrl, unitId, language) {
        axios.defaults.headers.common.Accept = 'application/json';
        axios.defaults.headers.common['Content-Type'] = 'application/json';

        if (token) {
            axios.defaults.headers.common.Authorization = `Bearer ${token}`;
        }

        axios.defaults.headers.common['MF-UNIT-ID'] = unitId;

        if (language) {
            axios.defaults.headers.common['Language'] = language;
        }

        this.axios = axios.create({
            baseURL: baseUrl,
            responseType: 'json',
        });
    }

    getAxios() {
        return this.axios;
    }

    sendSMS(params) {
        return this.getAxios().post('borrower/send-sms', params);
    }

    getDocumentTemplates(params) {
        return this.getAxios().post('document-template/list', params);
    }

    addDocument(params) {
        return this.getAxios().post('document/add', params);
    }

    updateDocument(params) {
        return this.getAxios().post('document/update', params);
    }

    getDocuments(params) {
        return this.getAxios().post('document/list', params);
    }

    getDocument(params) {
        return this.getAxios().post('document/get', params);
    }

    getBorrower(params) {
        return this.getAxios().post('borrower/get', params);
    }

    getBorrowerDocuments(params) {
        return this.getAxios().post('borrower/documents', params);
    }

    getBorrowerFileEncoded(params) {
        return this.getAxios().post('borrower/get-file-encoded', params);
    }

    paymentFeeForCreditUnion(params) {
        params = params || {};

        return this.getAxios().post('borrower/payment-fee-for-credit-union', params);
    }

    updateProfile(params) {
        return this.getAxios().post('borrower/update-profile', params);
    }

    updateBorrower(params) {
        return this.getAxios().post('borrower/update', params);
    }

    setFile(params) {
        return this.getAxios().post('borrower/set-file', params);
    }

    updateBorrowerProfile(params) {
        return this.getAxios().post('borrower-profile/update', params);
    }

    checkFieldsInBorrowerProfile(params) {
        return this.getAxios().post('borrower-profile/check-fields', params);
    }

    getCreditProducts() {
        return this.getAxios().post('credit-product/list');
    }

    getBorrowerFile(params) {
        return this.getAxios().post('borrower/get-file-encoded', params);
    }

    getBorrowerForm() {
        return this.getAxios().post('borrower-form/get');
    }

    getBorrowerContacts(params) {
        return this.getAxios().post('borrower-contact/list', params);
    }

    addBorrowerContact(params) {
        return this.getAxios().post('borrower-contact/add', params);
    }

    getBorrowerContact(params) {
        return this.getAxios().post('borrower-contact/get', params);
    }

    setMainBorrowerContact(params) {
        return this.getAxios().post('borrower-contact/set-main', params);
    }

    updateBorrowerContact(params) {
        return this.getAxios().post('borrower-contact/update', params);
    }

    sendActivationCodeForBorrowerContact(params) {
        return this.getAxios().post('borrower-contact/send-activation-code', params);
    }

    verifyActivationCodeForBorrowerContact(params) {
        return this.getAxios().post('borrower-contact/verify-activation-code', params);
    }

    getBorrowerCreditCards() {
        return this.getAxios().post('borrower-credit-card/list');
    }

    getVerifiedBorrowerCreditCards() {
        return this.getAxios().post('borrower-credit-card/list', {
            alias: 'bcc',
            filters: {
                function: 'and',
                where: [
                    {
                        operator: '!=',
                        key: 'bcc.verified_by',
                        value: null,
                    },
                ],
            },
        });
    }

    getDepositProducts() {
        return this.getAxios().post('/deposit/deposit-product/list');
    }

    getDepositApplications(params) {
        params = params || {};

        return this.getAxios().post('/deposit/deposit-application/list', params);
    }

    getDepositApplication(params) {
        return this.getAxios().post('/deposit/deposit-application/get', params);
    }

    assignDepositApplication(params) {
        return this.getAxios().post('/deposit/deposit-application/assign', params);
    }

    refillDepositApplication(params) {
        return this.getAxios().post('/deposit/deposit-application/refill', params);
    }

    applyDepositPromocode(params) {
        return this.getAxios().post('/deposit/deposit-promo-code/apply', params);
    }

    getDeposit(params) {
        return this.getAxios().post('/deposit/deposit/get', params);
    }

    getDeposits(params) {
        params = params || {};

        return this.getAxios().post('/deposit/deposit/list', params);
    }

    refillDeposit(params) {
        return this.getAxios().post('/deposit/deposit/refill-deposit', params);
    }

    getDepositPayoutSchedules(params) {
        params = params || {};

        return this.getAxios().post('/deposit/deposit-payout-schedule/list', params);
    }

    addDepositPayoutSchedules(params) {
        return this.getAxios().post('/deposit/deposit-payout-schedule/add', params);
    }

    addDepositApplication(params) {
        return this.getAxios().post('/deposit/deposit-application/add', params);
    }

    preCalculateDeposit(params) {
        return this.getAxios().post('/deposit/deposit-pre-calculator/get', params);
    }

    preCalculateCredit(params) {
        return this.getAxios().post('credit-pre-calculator/get', params);
    }

    addLoan(params) {
        return this.getAxios().post('loan/add', params);
    }

    getLoan(params) {
        return this.getAxios().post('loan/get', params);
    }

    getLoans(params) {
        return this.getAxios().post('loan/list', params);
    }

    signLoan(params) {
        return this.getAxios().post('loan/sign', params);
    }

    sendLoanRequestCodeForAssign(params) {
        return this.getAxios().post('loan/request-code-for-assign', params);
    }

    getCredits(params) {
        return this.getAxios().post('credit/list', params);
    }

    getCredit(params) {
        return this.getAxios().post('credit/get', params);
    }
}

export {
    BorrowerSDK,
    ManagerSDK,
    ResponseHandler,
    DepositApplicationStatus,
    LoanStatus,
    CreditStatus,
    DocumentTemplateTypes,
};
